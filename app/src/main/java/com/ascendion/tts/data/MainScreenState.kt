package com.ascendion.tts.data

data class MainScreenState(
    val isButtonEnabled:Boolean = true,
    val text:String = ""
)
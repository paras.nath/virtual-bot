package com.ascendion.tts.activity

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import com.ascendion.tts.screen.MainScreen
import com.ascendion.tts.ui.theme.TextToSpeechAppTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //enableEdgeToEdge()
        setContent {
            TextToSpeechAppTheme {
                MainScreen()
            }
        }
    }
}
